from django.test import TestCase, Client
from http import HTTPStatus
from .views import *
from .urls import *
from .models import rata_nilai

# Create your tests here.
class TestNilai(TestCase):
    def test_apakah_url_input_nilai_ada(self):
        response = Client().get('/nilai/input_nilai')
        self.assertEquals(response.status_code, HTTPStatus.OK)
    def test_apakah_url_daftar_nilai_ada(self):
        response = Client().get('/nilai/daftar_nilai')
        self.assertEquals(response.status_code, HTTPStatus.OK)
    def test_apakah_di_halaman_input_nilai_ada_text_dan_tombol_buttons(self):
        response = Client().get('/nilaimu/input_nilai')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Ayo list nilai kamu", html_kembalian)
        self.assertIn("Simpan", html_kembalian)
        self.assertIn("Kategori nilai :", html_kembalian)
        self.assertIn("Nilai :", html_kembalian)
        self.assertIn("Jika kamu sudah memiliki list nilai sebelumnya, ", html_kembalian)
        self.assertIn("klik disini", html_kembalian)