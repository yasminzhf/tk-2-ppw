from django.urls import path

from . import views

app_name = 'nilai'

urlpatterns = [
    path('input_nilai', views.index, name='index'),
    path('daftar_nilai',views.daftar_nilai, name="daftar_nilai"),
    path('hapus/<hapus_id>[0-9]', views.hapus, name="hapus")
]