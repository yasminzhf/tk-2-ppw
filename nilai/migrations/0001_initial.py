# Generated by Django 3.1.2 on 2020-12-28 04:56

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='rata_nilai',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_nilai', models.CharField(max_length=100)),
                ('isian_nilai', models.PositiveIntegerField(null=True)),
            ],
        ),
    ]
