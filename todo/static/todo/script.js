$(document).ready(function(){
    $("#createTaskForm").hide()

    $("#showFormButton").click(function(){
        $("#createTaskForm").show()
    })

    $("#createTaskForm").keypress(function(e){
        if(e.keyCode == 13){
            var serializedData = $("#createTaskForm").serialize();
            console.log(serializedData)
                $.ajax({
                    url : 'tambah/',
                    data: serializedData,
                    type:'post',
                    success: function(response){
                        console.log("bisa")
                        $("taskList").append('<tr id="lineData"><td>'+"response.activities"+'</td><td>'+ "response.activities.description" +'</td><td><a href="')
                        $("taskList").append("{% url 'todo:delete' activities.pk %}")
                        $("taskList").append('"><button class="btn btn-success hapus-btn" type="submit"><i class="fa fa-chevron-down" style="font-size:18px"></i></button></a></td></tr>')
                        console.log("bisa")
                        location.reload(true)
                    }
                })
        }
    })

    $("#createButton").click(function(){
        var serializedData = $("#createTaskForm").serialize();
    
        $.ajax({
            url : 'tambah/',
            data: serializedData,
            type:'post',
            success: function(response){
                $("taskList").append('<tr id="lineData"><td>'+response.activities.activity+'</td><td>'+ response.activities.description +'</td><td><a href="')
                $("taskList").append("{% url 'todo:delete' activities.pk %}")
                $("taskList").append('"><button class="btn btn-success hapus-btn" type="submit"><i class="fa fa-chevron-down" style="font-size:18px"></i></button></a></td></tr>')
            }
        })
    })

    $('#taskList tr').mouseenter(function (data) {
        $(this).css('background-color','#FFDD63');
    });

    $('#taskList tr').mouseleave(function (data) {
        $(this).css('background-color','white');
    });
})