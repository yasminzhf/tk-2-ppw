from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import *
from .urls import *
from .models import Todo
from .apps import TodoConfig
from hp_login.views import *
from hp_login.models import *
from hp_login.forms import CreateUserForm


# Create your tests here.
class TodoTest(TestCase):
    def test_todo_url_is_exist(self):
        response = Client().get('/todo/')
        self.assertEqual(response.status_code,302)

    def test_tambah_todo_url_is_exist(self):
        response = Client().get('/todo/tambah/')
        self.assertEqual(response.status_code,302)

    def test_func(self):
        found = resolve('/todo/')
        self.assertEqual(found.func, views.index)

    def test_app(self):
        self.assertEqual(TodoConfig.name, "todo")

    def test_setup(self):
        self.client = Client()
        self.response = self.client.get('/todo/')
        self.page_content = self.response.content.decode('utf8')
