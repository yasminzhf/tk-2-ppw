from django import forms
from .models import Todo
# from django.contrib.auth.models import User

class TodoForm(forms.ModelForm):
    activity = forms.CharField(label='')
    activity.widget.attrs.update({
        'class':'todo-form', 
        'required':True,
        })
    description = forms.CharField(label='')
    description.widget.attrs.update({
        'class':'todo-form',
        'required':True,
        })

    class Meta:
        model = Todo
        fields = ('activity','description')
      