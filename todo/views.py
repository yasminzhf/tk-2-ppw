from django.shortcuts import render,redirect
from .models import Todo
from .forms import TodoForm
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.forms.models import model_to_dict

# Create your views here.
@login_required
def index(request):
    activity = Todo.objects.all()
    formtambah= TodoForm()
    return render(request,'todo/todo.html',{
        'activity' : activity,
        'formtambah':formtambah,
    })

@login_required
def tambahTodo(request):
    formtambah= TodoForm(request.POST)
    if formtambah.is_valid():
        new_task=formtambah.save()
        print("berhasil save")
        return JsonResponse({'activity': model_to_dict(new_task)},status=200)
    else:
        return redirect('todo:index')
        
    

@login_required
def delete_todo(request, pk):
    activity = Todo.objects.get(pk=pk)
    activity.delete()
    return redirect('todo:index')
