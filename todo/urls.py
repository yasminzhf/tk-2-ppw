from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'todo'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name="index"),
    path('tambah/', views.tambahTodo, name="tambah"),
    path('<int:pk>/delete', views.delete_todo, name='delete'),
    
]