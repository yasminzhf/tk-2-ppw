from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Jadwal
from .forms import JadwalForms
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User


# Create your views here.

# def index(request):
#     return render(request, 'schedule/index.html')



@login_required(login_url='/accounts/login')
def index(request):  
    form = JadwalForms()
    # if request.method == 'POST':
    #     form = ContactModelForm(request.POST)
    #     if form.is_valid():
    #         form.save()
    #         return redirect('contact')
    log_user = request.user
    data = Jadwal.objects.filter(user=log_user)
    matkul = Jadwal.objects.all()
    # form = JadwalForms(request.POST)
    print(request.POST)
        
    if request.is_ajax():
        form = JadwalForms(request.POST)
        if form.is_valid():
            form.save()
            return JsonResponse({
                'message': 'success'
            })
    respon = {'form': form, 'data':data, 'matkul':matkul}
    return render(request, 'schedule/index.html', respon)


