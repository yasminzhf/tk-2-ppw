from django.contrib import admin
from .models import Jadwal

# Register your models here.

class QuestionAdmin(admin.ModelAdmin):
    fields = ['title', 'description']

admin.site.register(Jadwal, QuestionAdmin)