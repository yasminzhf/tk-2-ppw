# Create your tests here.
from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.contrib import auth
from django.urls import resolve
from .apps import ScheduleConfig
from . import views
import json

class SceduleTest(TestCase):
    def test_url_schedule_ada(self):
        response = Client().get('/schedule/')
        self.assertEqual(response.status_code,302)

    def test_func(self):
        found = resolve('/schedule/')
        self.assertEqual(found.func, views.index)

    def test_app(self):
        self.assertEqual(ScheduleConfig.name, "schedule")

    def test_setup(self):
        self.client = Client()
        self.response = self.client.get('/schedule/')
        self.page_content = self.response.content.decode('utf8')





