from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from .forms import CreateUserForm
from django.http import HttpResponse
from django.contrib.auth import authenticate, login
from django.contrib import messages


# Create your views here.
def index(request):
    return render(request,"hp_login/index.html")

def signup(request):
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            messages.success(request,'registration success')

            return redirect('hp_login:login')
    else:
        form = CreateUserForm()
    return render(request,"hp_login/registration.html",{
        'form':form
    })

def loginPage(request):
	if request.user.is_authenticated:
		return redirect('hp_login:index')
	else:
		if request.method == 'POST':
			username = request.POST.get('username')
			password =request.POST.get('password')

			user = authenticate(request, username=username, password=password)

			if user is not None:
				login(request, user)
				return redirect('hp_login:index')
			else:
				messages.info(request, 'username or password is incorrect')

		context = {}
		return render(request, 'registration/login.html', context)

