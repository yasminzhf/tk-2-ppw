from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout

# Create your tests here.
from .views import *
from .urls import *
from .forms import CreateUserForm

# Create your tests here.
class Hp_loginTestCase(TestCase):
    def test_is_home_page_exists(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'hp_login/index.html')

    def test_home_page_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_views_home(self):
        found = resolve('/')            
        self.assertEqual(found.func, index)

    def test_is_signup_page_exists(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'hp_login/registration.html')

    def test_signup_url_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_views_signup(self):
        found = resolve('/signup/')            
        self.assertEqual(found.func, signup)

    def test_is_login_page_exists(self):
        response = Client().get('/accounts/login/')
        self.assertTemplateUsed(response, 'registration/login.html')

    def test_login_url_exist(self):
        response = Client().get('/accounts/login/')
        self.assertEqual(response.status_code, 200)

    def test_views_login(self):
        found = resolve('/accounts/login/')            
        self.assertEqual(found.func, loginPage)

    def test_form(self):
        data={'username': 'yasminzhf','email': 'yasminzhafira@gmail.com',
        'password1': 'anakayam1000','password2': 'anakayam1000'}

        form = CreateUserForm(data=data)
        self.assertTrue(form.is_valid())

    def test_can_register_user(self):
        response=self.client.post(reverse('hp_login:signup'),{
            'email':'testemail@gmail.com',
            'username':'username',
            'password':'password',
            'password2':'password',
        },format='text/html')
        self.assertEqual(response.status_code,200)
        

    def test_login_success(self):
        self.client.post(reverse('hp_login:signup'),{
            'email':'testemail@gmail.com',
            'username':'username',
            'password':'password',
            'password2':'password',
        },format='text/html')
        User.is_active=True

        response= self.client.post(reverse('hp_login:loginPage'),{
            'email':'testemail@gmail.com',
            'username':'username',
            'password':'password',
            'password2':'password',
        },format='text/html')
        self.assertEqual(response.status_code,200)

    def test_redirect_user_authenticated(self):
        data={'username': 'yasminzhf','email': 'yasminzhafira@gmail.com',
        'password1': 'anakayam1000','password2': 'anakayam1000'}
        user = User.objects.create_user("yasminzhf29", "anakayam1000", "anakayam1000")
        self.client.force_login(user=user)
        response = self.client.get(reverse("hp_login:loginPage"))
        self.assertRedirects(response,"/")

        response=self.client.post(reverse('hp_login:loginPage'),data,format='text/html')
        self.assertRedirects(response,"/")

    def test_redirect_signup(self):
        data={'username': 'yasminzhf','email': 'yasminzhafira@gmail.com',
        'password1': 'anakayam1000','password2': 'anakayam1000'}
        response=self.client.post(reverse('hp_login:signup'),data,format='text/html')
        
        form = CreateUserForm(data=data)
        self.assertRedirects(response,"/accounts/login/")
